package br.com.mastertech.imersivo.estacionamento.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.estacionamento.model.Estacionamento;

@Repository
public interface EstacionamentoRepository extends CrudRepository<Estacionamento, Long>{

}
